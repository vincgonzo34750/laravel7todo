<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class TodosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Factory(App\Models\Todo::class, 25)->create();
        //factory(App\Models\Todo::class, 25)->create();
        //factory(App\Model\Todo::class, 25)->create();
    }
}
